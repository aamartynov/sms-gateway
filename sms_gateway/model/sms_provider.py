from odoo import models, api, fields


class SmsProvider(models.Model):
    _name = 'sms_gateway.sms_provider'

    provider_id = fields.Selection(
        selection=lambda _: _._provider_id(),
        string='Provider ID'
    )

    def send_message(self):
        raise NotImplementedError('Must implament in specific inherited model')

    def _provider_id(self):
        return []
