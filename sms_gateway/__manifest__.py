# -*- coding: utf-8 -*-

{
    'name': 'SMS gateway core module',
    'version': '10.0.18.01.20',
    'category': 'Phone',
    'license': 'AGPL-3',
    'author': "Martynov Alexander",
    'qweb': [],
    'data': [],
    'installable': True,
    'application': True
}
